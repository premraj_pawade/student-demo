const students = [{
    "id": 1,
    "first_name": "Bonnee",
    "last_name": "McIlmurray",
    "email": "bmcilmurray0@dailymotion.com"
  }, {
    "id": 2,
    "first_name": "Lenka",
    "last_name": "Solland",
    "email": "lsolland1@hao123.com"
  }, {
    "id": 3,
    "first_name": "Melisande",
    "last_name": "Jaram",
    "email": "mjaram2@cloudflare.com"
  }, {
    "id": 4,
    "first_name": "Clarisse",
    "last_name": "Coburn",
    "email": "ccoburn3@wufoo.com"
  }, {
    "id": 5,
    "first_name": "Tanner",
    "last_name": "Palethorpe",
    "email": "tpalethorpe4@vkontakte.ru"
  }, {
    "id": 6,
    "first_name": "Lonnie",
    "last_name": "Hansberry",
    "email": "lhansberry5@ask.com"
  }, {
    "id": 7,
    "first_name": "Dorena",
    "last_name": "Delgua",
    "email": "ddelgua6@mysql.com"
  }, {
    "id": 8,
    "first_name": "Moshe",
    "last_name": "Kingman",
    "email": "mkingman7@dion.ne.jp"
  }, {
    "id": 9,
    "first_name": "Dru",
    "last_name": "Astle",
    "email": "dastle8@woothemes.com"
  }, {
    "id": 10,
    "first_name": "Michaela",
    "last_name": "Allsupp",
    "email": "mallsupp9@t-online.de"
  }]


  //exporting object
  module.exports = students;